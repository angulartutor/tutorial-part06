import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

import { VillainService } from './villain.service';
import { AppComponent } from './app.component';
import { VillainDetailComponent } from './villain-detail/villain-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { VillainsComponent } from './villains/villains.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { VillainSearchComponent } from './villain-search/villain-search.component';


@NgModule({
  declarations: [
    AppComponent,
    VillainDetailComponent,
    MessagesComponent,
    VillainsComponent,
    DashboardComponent,
    VillainSearchComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [
    VillainService,
    MessageService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
