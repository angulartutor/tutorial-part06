import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';
// import 'rxjs/add/operator/map';
import { catchError, map, tap } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Villain } from './villain';
import { MessageService } from './message.service';


@Injectable()
export class VillainService {

  private static httpPostOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private static httpPutOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private static httpDeleteOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  // URL to web api
  private villainsUrl = 'api/villains';

  constructor(
    private http: HttpClient,
    private messageService: MessageService) {
  }

  private log(message: string) {
    this.messageService.add('VillainService: ' + message);
  }

  getVillains(): Observable<Villain[]> {
    this.log('Fetched villain list.');

    // return of(VILLAINS);
    return this.http.get<Villain[]>(this.villainsUrl)
    // ???? This throws type error. ?????
    // .pipe(
    //   tap(villains => this.log(`Fetched villains`)),
    // catchError(this.handlError<Villain[]>('getVillains', []))
    // );
  }

  getVillain(id: number): Observable<Villain> {
    // ????
    // return this.getVillains()
    //   .map(villains => villains.find(villain => villain.id === id));
    const url = `${this.villainsUrl}/${id}`;
    return this.http.get<Villain>(url).pipe(
      tap(_ => this.log(`Fetched villain id=${id}`)),
      catchError(this.handleError<Villain>(`getVillain id=${id}`))
    );
  }

  addVillain (villain: Villain): Observable<Villain> {
    return this.http.post<Villain>(this.villainsUrl, villain, VillainService.httpPostOptions).pipe(
      tap((villain: Villain) => this.log(`Added villain w/ id=${villain.id}`)),
      catchError(this.handleError<Villain>('addVillain'))
    );
  }

  updateVillain (villain: Villain): Observable<any> {
    return this.http.put(this.villainsUrl, villain, VillainService.httpPutOptions).pipe(
      tap(_ => this.log(`Updated villain id=${villain.id}`)),
      catchError(this.handleError<any>('updateVillain'))
    );
  }

  deleteVillain (villain: Villain | number): Observable<Villain> {
    const id = typeof villain === 'number' ? villain : villain.id;
    const url = `${this.villainsUrl}/${id}`;
  
    return this.http.delete<Villain>(url, VillainService.httpDeleteOptions).pipe(
      tap(_ => this.log(`Deleted villain id=${id}`)),
      catchError(this.handleError<Villain>('deleteVillain'))
    );
  }

  searchVillains(term: string): Observable<Villain[]> {
    term = term.trim();
    if (!term) {
      return of([]);
    }
    return this.http.get<Villain[]>(`${this.villainsUrl}/?name=${term}`).pipe(
      tap(_ => this.log(`Found villains matching "${term}"`)),
      catchError(this.handleError<Villain[]>('searchVillains', []))
    );
  }


  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
