import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Villain } from '../villain';
import { VillainService } from '../villain.service';

@Component({
  selector: 'my-villains',
  templateUrl: './villains.component.html',
  styleUrls: ['./villains.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VillainsComponent implements OnInit {
  selectedVillain: Villain;
  villains: Villain[];

  constructor(
    private router: Router,
    private villainService: VillainService) {

  }

  private _initVillains(): void {
    this.villainService.getVillains()
    .subscribe(villains => this.villains = villains);
  }

  ngOnInit(): void {
    this._initVillains();
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.villainService.addVillain({ name } as Villain)
      .subscribe(villain => {
        this.villains.push(villain);
      });
  }
  delete(villain: Villain): void {
    this.villains = this.villains.filter(h => h !== villain);
    this.villainService.deleteVillain(villain).subscribe();
  }

  onSelect(villain: Villain): void {
    this.selectedVillain = villain;
  }

  gotoDetail(): void {
    this.router.navigate(['/detail', this.selectedVillain.id]);
  }

}
